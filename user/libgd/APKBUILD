# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=libgd
pkgver=2.3.0
pkgrel=0
pkgdesc="Library for dynamic image creation"
url="http://libgd.github.io/"
arch="all"
options="!check"  # Multiple test suite failures. Assumes SSE+ math.
license="MIT"
depends=""
makedepends="autoconf automake bash fontconfig-dev freetype-dev
	libjpeg-turbo-dev libpng-dev libtool libwebp-dev tiff-dev zlib-dev
	"
# While the fontconfig/basic test checks for what happens if an empty
# fontlist is passed to gdImageStringFT(), there still needs to be at
# least one font installed on the system...
checkdepends="ttf-liberation"
subpackages="$pkgname-dev"
replaces="gd"
source="https://github.com/$pkgname/$pkgname/releases/download/gd-$pkgver/$pkgname-$pkgver.tar.xz"

# secfixes:
#   2.2.5-r1:
#     - CVE-2018-5711
#     - CVE-2018-1000222
#     - CVE-2019-6977
#     - CVE-2019-6978
#   2.2.5-r2:
#     - CVE-2018-14553
#   2.3.0-r0:
#     - CVE-2019-11038

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-werror
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	depends="$pkgname perl"
	replaces="gd-dev"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/bdftogd "$subpkgdir"/usr/bin
}

sha512sums="5b201d22560e147a3d5471010b898ad0268c3a2453b870d1267b6ba92e540cf9f75099336c1ab08217e41827ac86fe04525726bf29ad117e5dcbaef9a8d0622a  libgd-2.3.0.tar.xz"
