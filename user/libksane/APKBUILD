# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libksane
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE scanning library"
url="https://www.kde.org"
arch="all"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="sane-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev
	ktextwidgets-dev kwallet-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksane-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7ee4c83a28194f964b2b15f6d7a00d1b8f301e46a32522e9df99ded1ded8e47e11ca574f8199473060668dfd5e4208fdc80d028e0d413e25ffd98c439fb383f7  libksane-20.08.1.tar.xz"
