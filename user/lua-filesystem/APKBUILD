# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=lua-filesystem
_pkgname=luafilesystem
pkgver=1.8.0
_pkgver=$(printf '%s' "$pkgver" | tr . _)
_rockver=${pkgver%.*}-${pkgver##*.}
pkgrel=0
pkgdesc="Filesystem functions for Lua"
url="http://keplerproject.github.io/luafilesystem/"
arch="all"
license="MIT"
depends="lua5.3"
makedepends="lua5.3-dev"
source="$_pkgname-$pkgver.tar.gz::https://github.com/keplerproject/$_pkgname/archive/v$_pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$_pkgver"

build() {
	make CFLAGS="$CFLAGS $(pkg-config lua --cflags) -fPIC"
}

check() {
	LUA_CPATH=./src/?.so lua tests/test.lua
}

package() {
	rockdir="$pkgdir/usr/lib/luarocks/rocks-5.3/$_pkgname/$_rockver"
	make LUA_LIBDIR="$pkgdir/$(pkg-config --variable=INSTALL_CMOD lua)" install
	mkdir -p "$rockdir"
	echo 'rock_manifest = {}' > "$rockdir"/rock_manifest
}

sha512sums="79d964f13ae43716281dc8521d2f128b22f2261234c443e242b857cfdf621e208bdf4512f8ba710baa113e9b3b71e2544609de65e2c483f569c243a5cf058247  luafilesystem-1.8.0.tar.gz"
