# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kirigami-gallery
pkgver=20.08.1
pkgrel=0
pkgdesc="View examples of Kirigami components"
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
arch="all"
license="GPL-2.0-only"
depends="qt5-qtquickcontrols2 qt5-qtgraphicaleffects kirigami2"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev cmake extra-cmake-modules
	kirigami2-dev qt5-qttools-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild .
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="470a7d617005fb35e1ed7bad41dbe683bab1bf5019e872fa574e04d5b871d1a7b9c9d1315b3ee1f772de82970c0a3ec92995c88f8c659033d57893a639dfb7d7  kirigami-gallery-20.08.1.tar.xz"
