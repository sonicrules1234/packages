# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=calligra
pkgver=3.2.1
pkgrel=0
pkgdesc="KDE Office suite"
url="https://www.calligra.org/"
arch="all"
options="!check"  # Tests require X11
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev

	karchive-dev kcmutils-dev kcodecs-dev kcompletion-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev
	kguiaddons-dev ki18n-dev kiconthemes-dev kio-dev kitemviews-dev
	kjobwidgets-dev kdelibs4support-dev knotifications-dev kparts-dev
	knotifyconfig-dev kross-dev ktextwidgets-dev kwallet-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev sonnet-dev

	kactivities-dev khtml-dev kholidays-dev qt5-qtwebkit-dev

	boost-dev eigen-dev fontconfig-dev freetype-dev gsl-dev lcms2-dev
	libetonyek-dev libgit2-dev libodfgen-dev librevenge-dev libvisio-dev
	libwpd-dev libwpg-dev libwps-dev marble-dev poppler-dev poppler-qt5-dev
	qca-dev okular

	kcalendarcore-dev kcontacts-dev kdiagram-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/calligra/$pkgver/calligra-$pkgver.tar.xz
	braindump.patch
	ridiculous-typo.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		-DPRODUCTSET=desktop \
		-DQt5WebKitWidgets_FOUND=True \
		-DAPP_BRAINDUMP=True \
		-DBUILD_UNMAINTAINED=True \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="e53a939aa1b277c2291cfd626868c77085fc9cc7774df160b5b1afceb33314b4381fa8ffe03636cbde6e35cc2abd170a51e7f9c1a99191339313b9c9c3b1526f  calligra-3.2.1.tar.xz
786f02e5e21535c08343bee10ddf5d331dd34e778a117228edc010ca20dc497c285d3938a3166892f2faa20167133f3b64ab66f0a8b623ae5318601a7218359d  braindump.patch
3fa0700f471202eb8b76ded3928b884f43aa1c52e3fa6fc33b7fb62d5b1dcff1df2823723963a40f383e466eb7bebb1a66db14f2bd076a41f25b74f1a5f7f8d5  ridiculous-typo.patch"
