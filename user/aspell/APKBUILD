# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Sheila Aman <sheila@vulpine.house> 
pkgname=aspell
pkgver=0.60.8
pkgrel=0
pkgdesc="Libre spell checker software"
url="http://aspell.net/"
arch="all"
license="LGPL-2.0+"
depends="perl"
makedepends="ncurses-dev perl"
provides="aspell-utils"
subpackages="$pkgname-compat::noarch $pkgname-dev $pkgname-doc
	$pkgname-lang"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz"

# secfixes:
#   0.60.8-r0:
#     - CVE-2019-17544

build() {
	LIBS="-ltinfo" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--enable-dict-dir=/usr/share/$pkgname \
		--enable-pkgdatadir=/usr/share/$pkgname \
		--enable-compile-in-filters
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

compat() {
	pkgdesc="aspell compatibility scripts for ispell and unix spell"
	depends="$pkgname"
	install -d "$subpkgdir"/usr/bin
	cd "$pkgdir"/usr/share/$pkgname
	mv spell ispell "$subpkgdir"/usr/bin/
}

sha512sums="8ef4952c553b6234dfe777240d2d97beb13ef9201e18d56bee3b5068d13525db3625b7130d9f5122f7c529da0ccb0c70eb852a81472a7d15fb7c4ee5ba21cd29  aspell-0.60.8.tar.gz"
