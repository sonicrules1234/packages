# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=modemmanager-qt
pkgver=5.74.0
pkgrel=0
pkgdesc="Qt framework for ModemManager"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires MM running.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="modemmanager-dev"
makedepends="$depends_dev cmake doxygen extra-cmake-modules qt5-qtbase-dev
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/modemmanager-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c8dfaeca27a81b5a779f774b0edb4e4c670d1f92fc52f9e4863bacf725ae42f7d4b9dd813e47f56996a3427f0d813fb21162c609615e3523467e4fae0c4b6c1a  modemmanager-qt-5.74.0.tar.xz"
