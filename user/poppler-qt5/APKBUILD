# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=poppler-qt5
_realname=poppler
pkgver=0.83.0
pkgrel=0
_testver=01c92874
pkgdesc="PDF rendering library based on xpdf 3.0 (Qt 5 bindings)"
url="https://poppler.freedesktop.org/"
arch="all"
license="GPL-2.0+"
depends=""
depends_dev="$makedepends"
makedepends="libjpeg-turbo-dev cairo-dev libxml2-dev fontconfig-dev
	qt5-qtbase-dev poppler-dev~$pkgver lcms2-dev openjpeg-dev cmake"
subpackages="$pkgname-dev"
source="https://poppler.freedesktop.org/poppler-$pkgver.tar.xz
	https://dev.sick.bike/dist/poppler-test-$_testver.tar.gz"
builddir="$srcdir"/$_realname-$pkgver/build

# secfixes: poppler
#   0.77.0-r0:
#     - CVE-2019-9200
#     - CVE-2019-9631
#     - CVE-2019-9903
#     - CVE-2019-10872
#     - CVE-2019-10873
#     - CVE-2019-11026
#     - CVE-2019-12293
#   0.80.0-r0:
#     - CVE-2019-9959
#     - CVE-2019-14494

prepare() {
	default_prepare
	mkdir "$builddir"
}

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DTESTDATADIR="$srcdir/poppler-test-$_testver" \
		..
	make
}

check() {
	# check_qt5_annotations: fails on ppc64 and x86_64 as of 0.80.0-r0
	# FAIL!  : TestAnnotations::checkFontSizeAndColor() Compared values are not the same
	#   Actual   (textAnnot->contents()): "\u00C3\u00BE\u00C3\u00BF\u0000f\u0000o\u0000o\u0000b\u0000a\u0000r"
	#   Expected (contents)             : "foobar"
	#   Loc: [src/poppler-0.77.0/qt5/tests/check_annotations.cpp(100)]
	ctest --output-on-failure \
		-E check_qt5_annotations
}

package() {
	install -D -m644 poppler-qt5.pc "$pkgdir"/usr/lib/pkgconfig/poppler-qt5.pc
	cd qt5
	make DESTDIR="$pkgdir" install
}

sha512sums="bfde1e3cc4d9c626949aab770d84fccbe47bdde2331d3b9f5b98c24319eb573a5db62acbfa5a524b21ed0f195f8b163fee64677c2bd616d31d0bb219d1e66713  poppler-0.83.0.tar.xz
5275541ffa0fef9c55a0c02411947c610b2e7eb621f0a0fa9529810f8b09e2b0194c1da4b64eb9641b2c3af7b099e6bb7d1212b9087a21cf3af893090a10506b  poppler-test-01c92874.tar.gz"
