# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-nm
pkgver=5.18.5
pkgrel=0
pkgdesc="NetworkManager integration for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND GPL-2.0-only AND GPL-2.0+ AND LGPL-2.0+"
depends="prison-quick"
makedepends="cmake extra-cmake-modules kauth-dev kcodecs-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdeclarative-dev
	ki18n-dev kiconthemes-dev kio-dev kitemviews-dev kjobwidgets-dev
	knotifications-dev kpackage-dev kservice-dev kwallet-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev modemmanager-qt-dev
	networkmanager-dev networkmanager-qt-dev plasma-framework-dev
	prison-dev qca-dev qt5-qtbase-dev qt5-qtdeclarative-dev solid-dev"
subpackages="$pkgname-lang"
# We don't want to pull NM into plasma-meta, so we do this as a workaround.
install_if="plasma-desktop networkmanager"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-nm-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8f785f257f274fb9384bf2afbe4231a057c46706483e26dd2270a93a6fc97e6198916ff119d258a67d4f8bb73facb0ff67412f43e5fa66812962edb64331db4a  plasma-nm-5.18.5.tar.xz"
