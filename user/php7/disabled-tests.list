# Dumb failures
# Expects permissions on /etc to be 40755
ext/standard/tests/file/006_error.phpt
# Tests undefined behavior (integer underflow or overflow)
Zend/tests/dval_to_lval_32.phpt
Zend/tests/int_underflow_32bit.phpt
ext/date/tests/bug53437_var3.phpt
ext/date/tests/bug53437_var5.phpt
ext/date/tests/bug53437_var6.phpt
ext/exif/tests/bug79046.phpt
ext/exif/tests/float_cast_overflow.phpt

# General glibc/musl incompatibility related failures
# stdout printed in wrong order
ext/standard/tests/general_functions/ini_get_all.phpt
sapi/cgi/tests/005.phpt
# "Filename" instead of "File name" printed for ENAMETOOLONG
ext/standard/tests/strings/007.phpt
# glibc will throw EINVAL for popen with mode "rw" specifically,
# whereas musl only checks if the first character is 'r' or 'w'
ext/standard/tests/file/popen_pclose_error.phpt
# "Address in use" instead of "Address already in use" printed for EADDRINUSE
sapi/fpm/tests/socket-ipv4-fallback.phpt
# strerror differences
ext/sockets/tests/socket_strerror.phpt
ext/sockets/tests/socket_create_pair-wrongparams.phpt
# socket_addrinfo_explain has unexpected ai_canonname member
ext/sockets/tests/socket_addrinfo_explain.phpt

# locale related failures
# LC_NUMERIC unsupported
ext/standard/tests/strings/sprintf_f_3.phpt
ext/intl/tests/bug67052.phpt
ext/json/tests/bug41403.phpt
tests/lang/034.phpt
tests/lang/bug30638.phpt
# LC_ALL unsupported
ext/pcre/tests/locales.phpt
ext/standard/tests/array/locale_sort.phpt
ext/standard/tests/strings/setlocale_variation3.phpt
ext/standard/tests/strings/setlocale_variation4.phpt
ext/standard/tests/strings/setlocale_variation5.phpt
# LC_CTYPE unsupported
ext/standard/tests/strings/htmlentities02.phpt
ext/standard/tests/strings/htmlentities03.phpt
ext/standard/tests/strings/htmlentities04.phpt
ext/standard/tests/strings/htmlentities15.phpt
ext/standard/tests/strings/strtoupper.phpt
ext/fileinfo/tests/bug74170.phpt
# LC_MONETARY unsupported
ext/standard/tests/strings/moneyformat.phpt
ext/soap/tests/bugs/bug39815.phpt
# locale: command not found
ext/standard/tests/strings/setlocale_basic1.phpt
ext/standard/tests/strings/setlocale_basic2.phpt
ext/standard/tests/strings/setlocale_basic3.phpt
ext/standard/tests/strings/setlocale_variation1.phpt
ext/standard/tests/strings/setlocale_variation2.phpt
# setlocale allows "en_US.invalid"
ext/standard/tests/strings/setlocale_error.phpt
# bind_textdomain_codeset is a stub
ext/gettext/tests/gettext_bind_textdomain_codeset-retval.phpt
# //IGNORE
ext/iconv/tests/bug48147.phpt
ext/iconv/tests/bug76249.phpt
# //TRANSLIT
ext/iconv/tests/iconv_basic_001.phpt
# misc musl iconv incompatibilities
ext/iconv/tests/bug52211.phpt
ext/iconv/tests/eucjp2iso2022jp.phpt
ext/iconv/tests/iconv_mime_encode.phpt

# strftime and strptime related failures
# strftime %Z (timezone abbreviation) returns a single space
# This appears to be a bug in php and not musl
ext/date/tests/bug27780.phpt
ext/date/tests/bug32555.phpt
ext/date/tests/bug33532.phpt
# strptime returns tm with tm_wday and tm_yday == 0
# This appears to be a bug with *musl* and not php
# and also strftime returns a space for %Z as before
ext/standard/tests/time/strptime_basic.phpt
# strptime returning NULL when %Z is used (glibc extension)
ext/standard/tests/time/strptime_parts.phpt
# strftime("%q") returns false instead of some string
# This is because glibc will return "%q" and musl will return ""
ext/date/tests/009.phpt

# crypt() related failures
# crypt() returns "*" instead of "*0" or "*1"
ext/standard/tests/strings/bug51059.phpt
ext/standard/tests/crypt/bcrypt_invalid_algorithm.phpt
ext/standard/tests/crypt/bcrypt_invalid_cost.phpt
ext/standard/tests/strings/crypt_blowfish_variation1.phpt
ext/standard/tests/strings/crypt_blowfish_variation2.phpt
# crypt() returning incorrect results in general, in addition to the above
# and unexpected deprecation warning for invalid DES salt
ext/standard/tests/strings/crypt_blowfish.phpt
# crypt() has unexpected deprecation warning for invalid DES salt
ext/standard/tests/strings/crypt_des_error.phpt
# crypt() *missing* deprecation warnings for invalid DES salt
ext/standard/tests/crypt/des_fallback_invalid_salt.phpt

# These two are marked as XFAIL and do as such normally
# But with --enable-debug, they pass...
#sapi/fpm/tests/010.phpt
#sapi/fpm/tests/015.phpt

# Times out on builders but runs fine manually
ext/zlib/tests/bug67724.phpt
ext/zlib/tests/inflate_add_basic.phpt
sapi/cli/tests/upload_2G.phpt

# gd errors more verbose than expected
ext/gd/tests/bug39780_extern.phpt
ext/gd/tests/bug45799.phpt
ext/gd/tests/bug77973.phpt
ext/gd/tests/createfromwbmp2_extern.phpt
ext/gd/tests/libgd00086_extern.phpt

# no XPM support in system gd
ext/gd/tests/xpm2gd.phpt
ext/gd/tests/xpm2jpg.phpt
ext/gd/tests/xpm2png.phpt

# misc differences when using system gd
ext/gd/tests/bug43073.phpt
ext/gd/tests/bug48732-mb.phpt
ext/gd/tests/bug48732.phpt
ext/gd/tests/bug48801-mb.phpt
ext/gd/tests/bug48801.phpt
ext/gd/tests/bug53504.phpt
ext/gd/tests/bug65148.phpt
ext/gd/tests/bug73272.phpt
ext/gd/tests/bug73869.phpt
ext/gd/tests/bug79067.phpt
ext/gd/tests/bug79068.phpt

# requires a default route to be set, which isn't the case during
# network isolation
ext/sockets/tests/bug63000.phpt

# This should be skipped like long_columns.phpt is but it's missing the
# additional checks
ext/pdo_odbc/tests/max_columns.phpt

# enchant-2 doesn't support enchant_broker_(get|set)_dict_path
# https://news-web.php.net/php.internals/100882
ext/enchant/tests/bug53070.phpt

# Warning: zend_signal: handler was replaced
ext/readline/tests/libedit_callback_handler_install_001.phpt
ext/readline/tests/libedit_callback_handler_remove_001.phpt

# soap server's sum is not accumulating
ext/soap/tests/server009.phpt

# "Resource bundle source files are compiled with the genrb tool into a
# binary runtime form (.res files) that is portable among platforms with
# the same charset family (ASCII vs. EBCDIC) and **endianness**."
#
# Therefore these tests which use little-endian .res files will not work
# on our big endian arches...
ext/intl/tests/resourcebundle_arrayaccess.phpt
ext/intl/tests/resourcebundle_countable.phpt
ext/intl/tests/resourcebundle_create.phpt
ext/intl/tests/resourcebundle_individual.phpt
ext/intl/tests/resourcebundle_iterator.phpt
ext/intl/tests/resourcebundle_locales.phpt
ext/intl/tests/resourcebundle_traversable.phpt
