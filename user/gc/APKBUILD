# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gc
pkgver=8.0.6
pkgrel=0
pkgdesc="A garbage collector for C and C++"
url="https://hboehm.info/gc/"
arch="all"
license="MIT"
depends=""
makedepends="libatomic_ops-dev linux-headers"
subpackages="$pkgname-dev $pkgname-doc libgc++:libgccpp"
source="https://github.com/ivmai/bdwgc/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	if [ "$CLIBC" = "musl" ]; then
		export CFLAGS="$CFLAGS -D_GNU_SOURCE -DNO_GETCONTEXT -DUSE_MMAP -DHAVE_DL_ITERATE_PHDR"
	fi
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--datadir=/usr/share/doc/gc \
		--enable-cplusplus
	make
}

check() {
	if [ "$CARCH" = "ppc" ]; then
		make check || true
	else
		make check
	fi
}

package() {
	make DESTDIR="$pkgdir" install
}

libgccpp() {
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgccpp.* "$subpkgdir"/usr/lib/
}

sha512sums="2ea25003d585118e3ac0e12be9240e8195db511c6e94949f20453dc3cb771380bd5b956c04243b2a8ce31508587aa32de4f0f10a813577e6dbe8367688b7614e  gc-8.0.6.tar.gz"
