# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-browser-integration
pkgver=5.18.5
pkgrel=0
pkgdesc="Integrate Web browsers into the KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="cmake extra-cmake-modules kactivities-dev kconfig-dev kcrash-dev kdbusaddons-dev kfilemetadata-dev ki18n-dev kio-dev knotifications-dev krunner-dev purpose-dev qt5-qtbase-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-browser-integration-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
	rm -r "$pkgdir"/etc
}

sha512sums="096bfc0940fa3a40fe5145078fe801e2436779ac3333e953a68f7a68d3d592e3978606b70f1678c83a0748221e4d992ce14e0cb72c671f242d13db41390e8730  plasma-browser-integration-5.18.5.tar.xz"
