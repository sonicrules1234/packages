# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=erlang
pkgver=22.3
pkgrel=0
pkgdesc="Soft real-time system programming language"
url="https://www.erlang.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="autoconf automake flex libxml2-utils libxslt-dev m4 ncurses-dev
	openssl-dev perl unixodbc-dev"
subpackages="$pkgname-dev"
source="erlang-$pkgver.tar.gz::https://github.com/erlang/otp/archive/OTP-$pkgver.tar.gz
	fix-wx-linking.patch
	"
builddir="$srcdir/otp-OTP-$pkgver"

build() {
	./otp_build autoconf
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared-zlib \
		--enable-ssl=dynamic-ssl-lib \
		--enable-threads \
		--disable-hipe
	make
}

check() {
	export ERL_TOP=$builddir

	make release_tests

	for _header in erl_fixed_size_int_types.h \
		${CHOST}/erl_int_sizes_config.h \
		erl_memory_trace_parser.h; do
		cp erts/include/$_header erts/emulator/beam/
	done
	cd release/tests/test_server
	$ERL_TOP/bin/erl -s ts install -s ts smoke_test batch -s init stop
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="138c4807f1116ad507d5ce2899438aebf3e1d5503d0de1cf81535dfa2d7bf9224dc45adeeabe5e479bb83293002c0d03e7f78be9f93394e9b85f1d83a3381446  erlang-22.3.tar.gz
4331f4b9a8cd0787d79f26304415604118cf2f750cb2e98f3d0bb16dbf9e2b5e230b4b4b15fed4ac207a0ae83a0963adebd3f5e9423b98f0b7f733334669709d  fix-wx-linking.patch"
