# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=artikulate
pkgver=20.08.1
pkgrel=0
pkgdesc="Pronunciation trainer for languages"
url="https://www.kde.org/applications/education/artikulate/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	karchive-dev kconfig-dev kcrash-dev kdoctools-dev ki18n-dev
	kirigami2-dev knewstuff-dev kxmlgui-dev qt5-qtmultimedia-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/artikulate-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# TestCourseFiles needs X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E TestCourseFiles
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="907f23dd86dbd2de37f942a6c8446af1a62e0e1e805ee02eb7483cef74b7575a34cc0fbda52402ddf84f32c38fe8d15306cd8412aa8d449087af36327151a608  artikulate-20.08.1.tar.xz"
