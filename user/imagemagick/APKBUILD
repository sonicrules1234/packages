# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=imagemagick
pkgver=7.0.8.68
_abiver=7
_pkgver=${pkgver%.*}-${pkgver##*.}
pkgrel=0
pkgdesc="A collection of tools and libraries for many image formats"
url="http://www.imagemagick.org/"
arch="all"
options="libtool !check"  # needs actual helvetica font
license="Apache-2.0"
makedepends="zlib-dev libpng-dev libjpeg-turbo-dev freetype-dev fontconfig-dev
	perl-dev libwebp-dev libtool tiff-dev lcms2-dev fftw-dev libwebp-dev
	libxml2-dev librsvg-dev libraw-dev"
checkdepends="freetype fontconfig lcms2 graphviz"
subpackages="$pkgname-doc $pkgname-dev $pkgname-c++:_cxx $pkgname-libs"
source="https://dev.sick.bike/dist/ImageMagick-$_pkgver.tar.lz"
builddir="$srcdir/ImageMagick-${_pkgver}"

# secfixes:
#   7.0.8.59-r0:
#     - CVE-2019-13454

build() {
	# fix doc dir, Gentoo bug 91911
	sed -i -e \
		's:DOCUMENTATION_PATH="${DATA_DIR}/doc/${DOCUMENTATION_RELATIVE_PATH}":DOCUMENTATION_PATH="/usr/share/doc/imagemagick":g' \
		configure
	local _openmp=
	case "$CARCH" in
	s390x) _openmp="--disable-openmp"
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static \
		$_openmp \
		--with-threads \
		--with-x \
		--with-tiff \
		--with-png \
		--with-webp \
		--with-rsvg \
		--without-gslib \
		--with-modules \
		--with-xml \
		$_pic
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	if ! [ -e "$pkgdir"/usr/lib/libMagickCore-$_abiver.Q16HDRI.so ]; then
		error "Has ABI verision changed? (current is $_abiver)"
		return 1
	fi

	# we cannot let abuild delete the *.la files due to we need *.la
	# for the modules
	rm "$pkgdir"/usr/lib/*.la

	find "$pkgdir" -name '.packlist' -o -name 'perllocal.pod' \
		-o -name '*.bs' -delete

	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

_cxx() {
	pkgdesc="ImageMagick Magick++ library (C++ bindings)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libMagick++*.so.* "$subpkgdir"/usr/lib/
}

sha512sums="575398c6894a2590f6bd2e1c318ce09b12f019f7b8fd0bec9d0d5ee3e94d4f364c493e2e3f37a219b651caa8cb2336a4873dde6da7b5711da096ab520adc2b23  ImageMagick-7.0.8-68.tar.lz"
