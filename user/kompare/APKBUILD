# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kompare
pkgver=20.08.1
pkgrel=0
pkgdesc="View and merge differences between files"
url="https://kde.org/applications/development/org.kde.kompare"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdoctools-dev kiconthemes-dev kitemviews-dev kjobwidgets-dev kparts-dev
	kservice-dev ktexteditor-dev kwidgetsaddons-dev kwindowsystem-dev
	kxmlgui-dev libkomparediff2-dev solid-dev sonnet-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kompare-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="524a68563c18d33118f8b58aeb16faa718e8830fa071a2318ab0d8cfc218de269035650ca68e8415e5e6da5e037ba697bbe6d802ea68da76f9c66a93340f42ba  kompare-20.08.1.tar.xz"
