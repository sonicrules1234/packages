# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=digikam
pkgver=6.4.0
pkgrel=0
pkgdesc="Professional photo management and digital camera import"
url="https://www.digikam.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	qt5-qtsvg-dev kconfig-dev kwindowsystem-dev kxmlgui-dev ki18n-dev
	karchive-dev kio-dev qt5-qtwebkit-dev kcoreaddons-dev kservice-dev
	solid-dev kiconthemes-dev kfilemetadata-dev threadweaver-dev libpng-dev
	knotifyconfig-dev knotifications-dev akonadi-contacts-dev x265-dev
	libjpeg-turbo-dev tiff-dev zlib-dev boost-dev lcms2-dev expat-dev
	exiv2-dev flex bison libxml2-dev libxslt-dev eigen-dev libgphoto2-dev
	libksane-dev libkipi-dev glu-dev qt5-qtx11extras-dev jasper-dev
	opencv opencv-dev imagemagick-dev kcalendarcore-dev marble-dev"
# YES, both are needed. opencv-dev only pulls in -libs; CMake module in opencv
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/digikam/$pkgver/digikam-$pkgver.tar.xz
	opencv42.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_APPSTYLES=ON \
		-DENABLE_FACESENGINE_DNN=OFF \
		-DENABLE_MYSQLSUPPORT=OFF \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		-DENABLE_OPENCV3=ON \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b3b5e9903538d110613aa872c9215f5e7c19f7520a2b9060c24e686f20f72075378b447239dd3e17cb8860cdd35c699399994199b848757f0497ad25a7805e42  digikam-6.4.0.tar.xz
55b61bd67a25a4a1ec149205fd66c234a283a57f80c0070f95f085ae5e8f6cf5b9b5ed0a85a427ef58bd0f471d784555293b7fc61cf98eeb2ab84789da1979b1  opencv42.patch"
