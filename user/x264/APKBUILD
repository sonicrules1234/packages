# Maintainer: 
pkgname=x264
pkgver=20191105
pkgrel=0
pkgdesc="Library for encoding H264/AVC video streams"
url="https://www.videolan.org/developers/x264.html"
arch="all"
options="textrels"
license="GPL-2.0+"
depends=""
makedepends="bash coreutils libx11-dev perl"
case $CTARGET_ARCH in
	x86_64) makedepends="$makedepends nasm";;
esac
subpackages="$pkgname-dev $pkgname-libs"
source="https://download.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-$pkgver-2245-stable.tar.bz2"
builddir="$srcdir/$pkgname-snapshot-$pkgver-2245-stable"

build() {
	_asmopts=""
	case "$CARCH" in
		# ppc requires AltiVec, we run on G3s
		ppc | pmmx) _asmopts="--disable-asm" ;;
		# VSX was added in power8
		ppc64) _asmopts="--disable-vsx" ;;
	esac

	# note: not autotools
	CFLAGS="$(printf '%s' "$CFLAGS" | sed 's/-Os//')" \
	./configure \
		--host=$CHOST \
		--prefix=/usr \
		--enable-shared \
		--enable-static \
		$_asmopts \
		--enable-pic \
		--extra-cflags=-fno-aggressive-loop-optimizations
	make
}

check() {
	./x264 --version
}

package() {
	make DESTDIR="$pkgdir" \
		bindir=/usr/bin libdir=/usr/lib includedir=/usr/include \
		install
}

sha512sums="5a5bc2bd322af8544a6e7d89d6c56d1b609f8a69e177b20964fda1b5c8ad3a0ca77ff00b8ec47412a0c069afac677300c1d26aaaa704cf9be93df27f189171d3  x264-snapshot-20191105-2245-stable.tar.bz2"
