# Contributor: Dan Theisen <djt@hxx.in>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=hiredis
pkgver=1.0.0
pkgrel=0
pkgdesc="A minimalistic C client library for the Redis database"
url="https://github.com/redis/hiredis"
arch="all"
license="BSD-3-Clause"
options="!check"  # tests require a real redis server running
depends=""
makedepends="openssl-dev"
subpackages="$pkgname-dev"
source="hiredis-$pkgver.tar.gz::https://github.com/redis/hiredis/archive/refs/tags/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="eb56201121eecdbfc8d42e8c2c141ae77bea248eeb36687ac6835c9b2404f5475beb351c4d8539d552db4d88e933bb2bd5b73f165e62b130bb11aeff39928e69  hiredis-1.0.0.tar.gz"
