# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcachegrind
pkgver=20.08.1
pkgrel=1
pkgdesc="Profile data visualisation tool and call graph viewer"
url="https://kcachegrind.github.io/html/Home.html"
arch="all"
license="GPL-2.0-only"
depends="binutils graphviz"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	karchive-dev kconfig-dev kcoreaddons-dev kdoctools-dev ki18n-dev kio-dev
	kwidgetsaddons-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcachegrind-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a3bfb41f618ad360319372effd22d22c9486ecf8eae830f2a81f9e2253f453096cbc80d5c3b26c26f31449727ff854e9a79f5eb52b73f38786321a96cacd540d  kcachegrind-20.08.1.tar.xz"
