# Maintainer: Max Rees <maxcrees@me.com>
pkgname=gitlab-runner
pkgver=12.1.0
pkgrel=0
_testver=91956efe
_branch="${pkgver%.*}"
_branch="${pkgver%%.*}-${_branch#*.}-stable"
pkgdesc="GitLab runner for CI/CD jobs"
url="https://docs.gitlab.com/runner/"
# golang.org packages do not support ppc32
arch="all !ppc"
pkgusers="gitlab-runner"
pkggroups="gitlab-runner"
options="!check"  # Tests require network (git cloning)
license="MIT"
depends=""
makedepends="gcc-go"
subpackages="$pkgname-openrc"
install="$pkgname.pre-install"
source="https://gitlab.com/gitlab-org/gitlab-runner/-/archive/v$pkgver/gitlab-runner-v$pkgver.tar.gz
	https://dev.sick.bike/dist/gitlab-test-$_testver.tar.gz
	$pkgname.confd
	$pkgname.initd
	clean-tmpdirs.patch
	disable-crap.patch
	gccgo.patch
	posix-shell.patch
	posix-shell-escape.patch
	tests.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

unpack() {
	default_unpack
	mkdir "$builddir"/tmp
	mv gitlab-test "$builddir"/tmp
}

_make() {
	make "$@" VERSION="$pkgver" REVISION="v$pkgver" BRANCH="$_branch"
}

build() {
	_make build_simple
}

check() {
	# Even with network access and a clean chroot, there are some
	# tests that fail:
	#
	# gitlab.com/gitlab-org/gitlab-runner/commands/helpers :
	# TestCacheArchiverAddingUntrackedFiles
	# TestCacheArchiverAddingUntrackedUnicodeFiles

	# gitlab.com/gitlab-org/gitlab-runner/executors/shell :
	# TestBuildWithGitStrategyFetchWithLFS
	# TestBuildWithGitStrategyCloneWithLFS
	# TestBuildWithSubmoduleLFSPullsLFSObject
	#
	# gitlab.com/gitlab-org/gitlab-runner/network (error: use of
	# undefined type ‘RegisterRunnerParameters’)
	_make test
}

package() {
	install -Dm755 out/binaries/gitlab-runner \
		"$pkgdir"/usr/bin/gitlab-runner
	install -dm750 -g gitlab-runner \
		"$pkgdir"/etc/gitlab-runner
	install -dm750 -o gitlab-runner -g gitlab-runner \
		"$pkgdir"/var/lib/gitlab-runner

	install -Dm644 "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/$pkgname
	install -Dm755 "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
}

sha512sums="1b3f06b7000252631bec04a58474dda826b0fb8d28c4144051a231fba5ef0c5c4b6e0f9ca6bdf9bf9b16b9117cbcf6385eb7bac85bda7b9f99de4300cbcc2066  gitlab-runner-v12.1.0.tar.gz
7bccab81aa1cb88c6a0f83a6d3dfc509e6f9ecb4d0168a5b30cc4e8fdd823294b95fe3633e09757905466f83bb3bf73b1d28fac475dff2dc3164604db4b5b995  gitlab-test-91956efe.tar.gz
f8a0c7e77cd563d7e5b4fb2319181ceab8de44c555cefa4803e985ad3ac6a9fea7bca469d80b28974edcfb65f2095d019bfdf0abd8a3221464276a8d89debc0d  gitlab-runner.confd
7f785a65b46919f6a6695e25fdc414ee3204eb9aa1a3735e7884575434af94b20a48f2fe7a1fb787473de23a7bd2d56bc1e9c1fc6b522c6f0c05c47b3d37ab1c  gitlab-runner.initd
f17a6b5053933d83d90272be7e98d4abf8d63e8a9c5ad30bbb61a29dbfa1e1b55421ba14be9f7db17cae30b2ff04efcb5da68543a2fa3d0e3e1882bc128cac06  clean-tmpdirs.patch
a32b7a6fa28c7f5bcb505308e615f17de179c15ce8c2ef73078143204195e36d99b18489114ce694c3d821378c1e9ccb26a999a50372b97df163e2512080f07e  disable-crap.patch
4bc8f827cbcec212fcb1c5598aaf725138efa9a1de699d6261a65207cca7ef46f441edc02987efdd6d7c6e62943fda8f3f2a5a596d8d33b49fb78ba5df612e8b  gccgo.patch
444adec507c5eebcfae268f2f475d29316119f30750f06877a91dff56942d0e4d34ed3011b774b0d7acd59acfe2ddbe75082b3cd5aebd72ddb4703fd083ad301  posix-shell.patch
05d12046448c79a12bf92974089ff97781304083f7bf35abb0b0c0f990c536bd6ff51652e16035a38e4ba5620dcad6e051ecdf4a80eb62039a15371073e525b5  posix-shell-escape.patch
dae9c3f86fdf21bd41202fc2c0bc7dd764078ea9215d7ddbac1f7bc47b0943f7a912250a3006122f74ed50052378ed2c946eefe5c1ce8400cd682b183221025a  tests.patch"
