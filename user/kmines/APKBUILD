# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmines
pkgver=20.08.1
pkgrel=0
pkgdesc="Classic Minesweeper game"
url="https://games.kde.org/game.php?game=kmines"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfigwidgets-dev
	kconfig-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kdoctools-dev
	ki18n-dev ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmines-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2e991306367697af2657ab9ca0a8251d7ed9d43e7e97893fa5267c52f4c26686a16a997d3f75b71fc4d54af4a1318d97c1f69ba3b694697a4b2b79eae1942294  kmines-20.08.1.tar.xz"
