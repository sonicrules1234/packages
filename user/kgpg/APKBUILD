# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kgpg
pkgver=20.08.1
pkgrel=0
pkgdesc="Simple interface for GnuPG, a powerful encryption utility"
url="https://utils.kde.org/projects/kgpg/"
arch="all"
options="!check"  # Tests require X11 now.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kcodecs-dev
	kdoctools-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev ki18n-dev
	kiconthemes-dev kjobwidgets-dev kio-dev knotifications-dev kservice-dev
	ktextwidgets-dev kxmlgui-dev kwidgetsaddons-dev kwindowsystem-dev
	akonadi-contacts-dev kcontacts-dev gpgme-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgpg-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="326a440b1fc12520c46e516d609697327d46eac5e62ef012411f3b3dd170256d4d72b2ddd741ca985c36934c63d999da80d6f6850db8a22fb640541dac918b2b  kgpg-20.08.1.tar.xz"
