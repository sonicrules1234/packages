# Contributor: Dan Theisen <djt@hxx.in>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=ruby-reline
_gemname=${pkgname#ruby-}
pkgver=0.2.7
pkgrel=0
pkgdesc="Reline is compatible with the API of Ruby's stdlib 'readline', GNU Readline and Editline"
url="https://github.com/ruby/reline"
arch="all"
license="BSD-2-Clause"
depends="ruby"
checkdepends="ruby-rspec"
makedepends="ruby-rake"
source="ruby-reline-$pkgver.tar.gz::https://github.com/ruby/$_gemname/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_gemname-$pkgver"

build() {
	gem build $_gemname.gemspec
}

check() {
	rspec spec
}

package() {
	gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"
	
	gem install --local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname
	
	# Remove unnecessary files and empty directories.
	cd "$gemdir"
	rm -r cache build_info doc
}

sha512sums="3543207d79a9cb5293cefb4771a5d30bfa158915f2ba84db1cd04b1c89b21288542e52a2e43e2ca057bad91c926052dc3ba404319a5eb4044ab054b6c3cab465  ruby-reline-0.2.7.tar.gz"
