# Contributor: Jose-Luis Rivas <ghostbar@riseup.net>
# Conttributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libuv
pkgver=1.28.0
pkgrel=0
pkgdesc="Cross-platform asychronous I/O"
url="https://libuv.org"
arch="all"
license="MIT AND BSD-2-Clause AND BSD-3-Clause AND ISC"
makedepends="automake autoconf libtool linux-headers"
subpackages="$pkgname-dev"
source="https://dist.libuv.org/dist/v$pkgver/$pkgname-v$pkgver.tar.gz
	disable-process-priority.patch
	disable-setuid-test.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	sh autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make CFLAGS="$CFLAGS -D__USE_MISC" BUILDTYPE=Release
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="d7f635ab99569e96db9ae97d29a302f5eec1fd75c71b035ec597a6b978a3fc797a37c7406fed81a27d4d706fe21cbfe919d829d6dae67399cd5cddd107ad6949  libuv-v1.28.0.tar.gz
58bc259a0f703fa6b8dda14909f718e14dfb9ddc9039ae13378f7e32e0ff7096774cbfb2964a0abb06f8a64a71cf298cc77b7475927df0fb50ce3f0e414a7787  disable-process-priority.patch
478d25c8905cd393b9ced0f1b16e70794a7ef20fb9eb212fd74e50beca3f5a33a6a5267616abecf470426ed3d00efec51df468745ff43c0de05c0ad8234f1eb3  disable-setuid-test.patch"
