# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=rsync
pkgver=3.2.3
pkgrel=0
pkgdesc="File transfer program to keep remote files in sync"
url="https://rsync.samba.org/"
arch="all"
license="GPL-3.0+"
depends=""
checkdepends="fakeroot"
makedepends="perl acl-dev attr-dev lz4-dev openssl-dev popt-dev zlib-dev
	zstd-dev"
subpackages="$pkgname-doc $pkgname-openrc rrsync::noarch"
source="https://download.samba.org/pub/$pkgname/$pkgname-$pkgver.tar.gz
	rsyncd.initd
	rsyncd.confd
	rsyncd.conf
	rsyncd.logrotate
	CVE-2020-14387.patch
	"

# secfixes:
#   3.2.3-r0:
#     - CVE-2020-14387
#   3.1.3-r2:
#     - CVE-2016-9840
#     - CVE-2016-9841
#     - CVE-2016-9842
#     - CVE-2016-9843

build() {
	# Force IPv6 enabled, upstream bug https://bugzilla.samba.org/show_bug.cgi?id=10715
	CFLAGS="$CFLAGS -DINET6" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-acl-support \
		--enable-xattr-support \
		--with-included-zlib=no \
		--disable-xxhash
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -D -m 755 "$srcdir"/rsyncd.initd "$pkgdir"/etc/init.d/rsyncd
	install -D -m 644 "$srcdir"/rsyncd.conf "$pkgdir"/etc/rsyncd.conf
	install -D -m 644 "$srcdir"/rsyncd.confd "$pkgdir"/etc/conf.d/rsyncd
	install -D -m 644 "$srcdir"/rsyncd.logrotate "$pkgdir"/etc/logrotate.d/rsyncd

}

rrsync() {
	pkgdesc="Restricted rsync, restricts rsync to a subdir declared in .ssh/authorized_keys"
	depends="rsync perl"

	install -D -m 755 "$builddir"/support/rrsync "$subpkgdir"/usr/bin/rrsync
}

sha512sums="48b68491f3ef644dbbbfcaec5ab90a1028593e02d50367ce161fd9d3d0bd0a3628bc57c5e5dec4be3a1d213f784f879b8a8fcdfd789ba0f99837cba16e1ae70e  rsync-3.2.3.tar.gz
638d87c9a753b35044f6321ccd09d2c0addaab3c52c40863eb6905905576b5268bec67b496df81225528c9e39fbd92e9225d7b3037ab1fda78508d452c78158f  rsyncd.initd
c7527e289c81bee5e4c14b890817cdb47d14f0d26dd8dcdcbe85c7199cf27c57a0b679bdd1b115bfe00de77b52709cc5d97522a47f63c1bb5104f4a7220c9961  rsyncd.confd
3db8a2b364fc89132af6143af90513deb6be3a78c8180d47c969e33cb5edde9db88aad27758a6911f93781e3c9846aeadc80fffc761c355d6a28358853156b62  rsyncd.conf
b8d6c0bb467a5c963317dc55478d2c10874564cd264d943d4a42037e2fce134fe001fabc92af5c6b5775e84dc310b1c8da147afaa61c99e5663c36580d8651a5  rsyncd.logrotate
cebd8b23db8fb095e35e133ab828efecc385e159e429b3c2366f411572cdebb3444be0f60b42b2ce3d34476f1ebb4f194933d699978db385ac40c4fba6767991  CVE-2020-14387.patch"
